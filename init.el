;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments


(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-auto-commit-chars "")
 '(company-auto-complete-chars "")
 '(display-line-numbers nil)
 '(display-time-24hr-format t)
 '(elfeed-feeds '("http://www.svt.se/nyheter/rss.xml"))
 '(elfeed-goodies/entry-pane-position 'bottom)
 '(erc-nick nil)
 '(erc-server "irc.quakenet.org")
 '(frame-background-mode 'dark)
 '(global-whitespace-mode nil)
 '(global-whitespace-newline-mode nil)
 '(image-animate-loop t)
 '(image-dired-main-image-directory "~/images/")
 '(image-scaling-factor 'auto)
 '(menu-bar-mode nil)
 '(org-agenda-files
   '("~/cs/118/skrivuppgift/skrivuppgift.org" "~/gtd/inbox.org" "~/gtd/gtd.org" "~/gtd/tickler.org"))
 '(org-format-latex-options
   '(:foreground default :background default :scale 1.0 :html-foreground "Black" :html-background "Transparent" :html-scale 1.0 :matchers
                 ("begin" "$1" "$" "$$" "\\(" "\\[")))
 '(org-hide-leading-stars t)
 '(org-html-doctype "html5")
 '(org-html-html5-fancy t)
 '(org-structure-template-alist
   '(("el" . "src emacs-lisp")
     ("py" . "src python :results output")
     ("a" . "export ascii")
     ("c" . "center")
     ("C" . "comment")
     ("e" . "example")
     ("E" . "export")
     ("h" . "export html")
     ("l" . "export latex")
     ("q" . "quote")
     ("s" . "src")
     ("v" . "verse")))
 '(package-selected-packages
   '(glsl-mode latex-pretty-symbols org-roam-bibtex org-roam async helm org-ref racket-mode tuareg taureg f odoc merlin-company merlin caml magit auctex-latexmk use-package c0-mode ob-sml js2-mode projectile company-mode htmlize sml-mode transmission flycheck elpy slime dashboard paradox which-key avy elfeed ##))
 '(paradox-github-token t)
 '(read-answer-short t)
 '(show-paren-mode t)
 '(tool-bar-mode nil)
 '(window-divider-default-bottom-width 1)
 '(window-divider-default-places 'right-only)
 '(window-divider-default-right-width 1)
 '(window-divider-mode nil)
 '(window-sides-vertical t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "#022C41" :foreground "white" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 151 :width normal :foundry "1ASC" :family "Liberation Mono"))))
 '(company-scrollbar-bg ((t (:background "white"))))
 '(company-scrollbar-fg ((t (:background "DodgerBlue4"))))
 '(company-template-field ((t (:background "DodgerBlue4" :foreground "black"))))
 '(company-tooltip ((t (:background "DodgerBlue4" :foreground "white"))))
 '(company-tooltip-common ((t (:foreground "white"))))
 '(dired-directory ((t (:inherit font-lock-function-name-face))))
 '(dired-flagged ((t (:inherit error :foreground "brown3" :underline t))))
 '(erc-input-face ((t (:foreground "light green"))))
 '(erc-my-nick-face ((t (:foreground "forest green" :weight bold))))
 '(erc-notice-face ((t (:foreground "pale turquoise" :weight bold))))
 '(erc-timestamp-face ((t (:foreground "white" :underline nil :weight bold))))
 '(font-lock-comment-face ((t (:foreground "#57a64a"))))
 '(font-lock-constant-face ((t (:foreground "IndianRed1"))))
 '(font-lock-function-face ((t (:foreground "SkyBlue1"))))
 '(font-lock-function-name-face ((t (:foreground "wheat"))))
 '(font-lock-keyword-face ((t (:foreground "medium spring green"))))
 '(font-lock-string-face ((t (:foreground "SpringGreen1"))))
 '(font-lock-type-face ((t (:foreground "orange1"))))
 '(link ((t (:foreground "aquamarine" :underline t))))
 '(menu ((t (:inverse-video nil))))
 '(minibuffer-prompt ((t (:foreground "light sea green"))))
 '(mode-line ((t (:background "#022C41" :foreground "#83d3fb"))))
 '(mode-line-inactive ((t (:inherit mode-line :background "#022C41" :foreground "grey80" :box (:line-width -1 :color "#022C41") :weight light))))
 '(org-level-1 ((t (:inherit outline-1 :foreground "pale green" :slant italic))))
 '(org-level-2 ((t (:inherit outline-2 :foreground "light goldenrod" :slant italic))))
 '(org-level-3 ((t (:inherit outline-3 :foreground "sky blue" :slant italic))))
 '(org-meta-line ((t (:inherit font-lock-comment-face :foreground "burlywood3"))))
 '(proced-marked ((t (:inherit error :foreground "firebrick3"))))
 '(region ((t (:background "#022131"))))
 '(slime-repl-prompt-face ((t (:inherit font-lock-keyword-face :foreground "orange"))))
 '(tool-bar ((t (:background "dark cyan" :foreground "white" :box (:line-width 1 :style released-button)))))
 '(tooltip ((t (:inherit variable-pitch :background "dark cyan" :foreground "white"))))
 '(vertical-border ((((type tty)) (:inherit mode-line-inactive :background "#83d3fb"))))
 '(window-divider ((t (:foreground "light salmon"))))
 '(window-divider-first-pixel ((t (:foreground "firebrick")))))

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; loads org-mode config file
(org-babel-load-file (expand-file-name "~/.emacs.d/config.org"))
